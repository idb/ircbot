# IRC Mail Bot 

This simple bot is created with Node.js, and it's sole purpose is to receive new 
email notifications through IMAP in order to forward them to IRC.

### Installing

Using npm (note that it will be located in `node_modules/ircmailbot`:
```
npm install ircmailbot
```

Using git:
```
git clone https://git.snt.utwente.nl/idb/ircbot.git
cd ircbot
npm install
```

### Configuration

Start by copying the `config.json.dist` to `config.json`, and edit the values:

##### IMAP

* `user`: IMAP username
* `password`: IMAP password
* `host`: IMAP server (like smtp.gmail.com)
* `port`: IMAP port (like 993)
* `tls`: Either `true` or `false`, enables TLS support.
 
##### Irker

* `host`: Irker instance hostname/ip address/localhost
* `port`: Irker instance port
* `to`: Irker target descriptor

The irker target descriptor looks like `irc://irc-url/target`. The irc-url consist of 
the hostname and port of the IRC-server (`hostname:port`), where the target indicates 
the channel/nick to communicate with. Some target examples:
* `#channel` for a simple channel
* `channel?key=xxx` for a channel protected by a key
* `nick,isnick` for a simple nick
  
##### Function

* `appname`: Instance name, used in logging only
* `privacy`: Value should be either `0`, `1` or `2`, indicating the privacy level
  * `0`: Only a simple notification is sent
  * `1`: The subject is added to the notification
  * `2`: The sender is added to the notification
* `readonly`: Either `true` or `false`, whether to open de IMAP box with write permissions. 
Write permissions are only required when you want to move messages after notification.
* `move`: Either `true` of `false`, whether to move messages which have triggered their notification.
* `moveTo`: If `move` is set to `true`, this value will indicate the folder where the messaged is moved to.

### Running

Simple run:
```
node app.js
```

Forever run:
```
forever start app.js
```

### Automatic run

If you want to have this bot run automatically on system start and restart on crash, 
make sure to install the `forever` module (`sudo npm install -g forever`) and create 
the following systemd service file (in `/etc/systemd/system/ircmailbot.service`). 
In this example, the bot has been installed in `/var/www/bot/`.

```
[Unit]
Description=IRC mail bot NodeJS server
After=network.target

[Service]
User=www-data
Type=oneshot
RemainAfterExit=yes
ExecStart=/usr/bin/forever start /var/www/bot/app.js
ExecStop=/usr/bin/forever stopall


[Install]
WantedBy=multi-user.target
```
