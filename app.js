console.log(getDateTime() + "[ info ] Preboot");

var Imap = require('imap');
var config = require('./conf.json');
var inbox = null;

console.log(getDateTime() + "[ info ] Booting " + config.function.appname + " mail-client!");

var imapReconnectTimer;
function imapReconnect(){
  inbox = null;
  imapReconnectTimer = setTimeout(function () {
    console.log(getDateTime() + "[ info ] Reconnecting IMAP...");
    imap.connect();
  }, 30000);
}

var imap = new Imap({
  user: config.imap.user,
  password: config.imap.password,
  host: config.imap.host,
  port: config.imap.port,
  tls: config.imap.tls,
  tlsOptions: {
    servername: config.imap.host,
  }
});

imap.on('ready', function () {
  clearInterval(imapReconnectTimer);
  console.log(getDateTime() + "[ info ] Connected to IMAP!");
  imap.openBox('INBOX', config.function.readonly, function (err, box) {
    inbox = box;
  });
});

imap.on('error', function (err) {
  console.log(getDateTime() + "[ err  ] IMAP error: " + err);
  console.log(getDateTime() + "[ info ] Resetting IMAP connection...");
  imap.end();
  // imap.end() does not trigger the end hook -.-
  imapReconnect();
});

imap.on('end', function () {
  console.log(getDateTime() + "[ info ] Disconnected from IMAP");
  imapReconnect();
});

imap.on('mail', function (num) {
  console.log(getDateTime() + "[ info ] Received " + num + " email(s) on IMAP box");

  var f;
  if (!config.function.move && inbox === null) {
    // This is only true on startup, so we probably already notified about these messages
    sendIrcMessage("Skipping notification of " + num + " messages due to startup.");
    return;
  } else if (config.function.move || inbox === null) {
    f = imap.seq.fetch('1:' + num, {
      bodies: 'HEADER.FIELDS (FROM SUBJECT)',
      struct: true
    });
  } else {
    f = imap.seq.fetch(inbox.messages.total + ':*', {
      bodies: 'HEADER.FIELDS (FROM SUBJECT)',
      struct: true
    });
  }
  f.on('message', function (msg, seqno) {
    msg.on('body', function (stream, info) {
      var buffer = '';
      stream.on('data', function (chunk) {
        buffer += chunk.toString('utf8');
      });
      stream.once('end', function () {
        var msg = Imap.parseHeader(buffer);
        switch(config.function.privacy) {
          default:
          case 0:
            var irk = "MAIL: A new message has arrived.";
            break;
          case 1:
            var irk = "MAIL: Message '" + msg.subject[0] + "' arrived.";
            break;
          case 2:
            var irk = "MAIL: Message '" + msg.subject[0] + "' from '" + msg.from[0] + "' arrived.";
            break;
        }
        sendIrcMessage(irk);
      });
    });
  });
  f.on('end', function () {
    if (config.function.move) {
      imap.seq.move('1:' + num, config.function.moveTo, function (err) {
        if (typeof err !== 'undefined' && err) {
          console.log(getDateTime() + '[ err  ] IMAP move error: ' + err);
        } else {
          console.log(getDateTime() + '[ info ] Message notification sent and message archived');
        }
      });
    }
  });
});

process.on('uncaughtException', function (err) {
  console.log(getDateTime() + "[ err  ] Uncaught error:");
  console.log(err);
});

console.log(getDateTime() + "[ info ] Connecting to IMAP...");
imap.connect();

function getDateTime() {

  var date = new Date();

  var hour = date.getHours();
  hour = (hour < 10 ? "0" : "") + hour;

  var min = date.getMinutes();
  min = (min < 10 ? "0" : "") + min;

  var sec = date.getSeconds();
  sec = (sec < 10 ? "0" : "") + sec;

  var year = date.getFullYear();

  var month = date.getMonth() + 1;
  month = (month < 10 ? "0" : "") + month;

  var day = date.getDate();
  day = (day < 10 ? "0" : "") + day;

  return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec + " ";

}

function sendIrcMessage(message) {

  var s = require('net').Socket();
  s.connect({
    host: config.irker.host,
    port: config.irker.port,
    family: 4,
  });
  s.write(JSON.stringify({
    to: config.irker.to,
    privmsg: message
  }));
  s.end();

}
